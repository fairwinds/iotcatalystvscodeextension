# IoT Catalyst DevOps - Design, Deploy, Debug

[View in Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=IoTCatalystVSCodeExtensionPublisher.IoTCatalystDevOps)

# Table of Contents
- [Introduction](#introduction)
- [Connecting to a Remote Host](#connecting-to-a-remote-host)
- [Creating and instantiating a Digital Thing](#creating-and-instantiating-a-digital-thing)
- [Adding Features to the Digital Thing](#adding-features-to-the-digital-thing)
- [Editing the Digital Thing Code](#editing-the-digital-thing-code)
- [Notify IoT Catalyst about changes in the Digital Thing and its Container](#notify-iot-catalyst-about-changes-in-the-digital-thing-and-its-container)
- [Debugging the Container](#debugging-the-container)

## Introduction
In this example, we will make use the IoT Catalyst DevOps extension to do the following:
1. Connect to a remote host
2. Create a new Digital Thing and instantiate it by mounting a new container on the remote host
3. Add some features and custom code to the Digital Thing
4. Deploy the updated Digital Thing to IoT Catalyst Studio
5. Debug the Digital Thing code

## Connecting to a Remote Host

Once the Extension has been installed, let's open an empty VSCode Workspace and go to the "IoT Catalyst DevOps" panel.

![Select the IoT Catalyst DevOps Panel](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/1-open-extension-panel.png "Select the IoT Catalyst DevOps Panel")

Here, click on "Development Manager" > "Connect to a Remote Host".

![Remote Host Connection](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/2-connect-to-remote.png "Remote Host Connection")

VSCode will let you select a remote host among those defined in your SSH remote configuration.

Once selected, the editor will reopen in the remote host (in the example below it's called "RaspberryPi").

Here, make sure that the DevOps extension is installed also on the remote host: using the left sidebar, click on "Extensions" and search for "IoT Catalyst DevOps". If needed, click on "Install in SSH: \<name of the remote host\>" like in the figure below.

![Install Extension on Remote Host](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/3-make-sure-extension-remotely-installed.png "Install Extension on Remote Host")

If everything succeeds, you should see the IoT Catalyst DevOps icon appearing in the left sidebar.

## Creating and instantiating a Digital Thing

Now that we've installed DevOps on the remote host, we're ready to create a Digital Thing. Using the IoT Catalyst DevOps panel, click on "Development Manager" > "Create a new Digital Thing".

![Create New Digital Thing](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/3-make-sure-extension-remotely-installed.png "Create New Digital Thing")

Here, you may be prompted for IoT Catalyst Studio credentials if not found locally by the extension. 

Once done, you will be asked to choose a Designer which acts as a blueprint for your Digital Thing. For this example, let's choose "Generic Device".

![Choosing a Designer](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/5-create-new-dt-select-designer.png "Choosing a Designer")

Now let's enter a name and a description for the new Digital Thing. I will call it "my_awesome_digitalthing" but feel free to choose the name you like the most!

![Enter Digital Thing Name](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/6-create-new-dt-name.png "Enter Digital Thing Name")

![Enter Digital Thing Description](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/7-create-new-dt-description.png "Enter Digital Thing Description")

Once done, the Digital Thing "my_awesome_digitalthing" has been uploaded on the IoT Catalyst Library but it's not executing yet. It's time to instantiate it on the remote host by mounting a new Container.

Using the IoT Catalyst DevOps panel, click on "Development Manager" > "Mount a new Container".

![Mount a new Container](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/8-mount-new-container-button.png "Mount a new Container")

You will be prompted to choose a Digital Thing to be instantiated, among the ones available in the IoT Catalyst library which are also compatible with the remote host architecture. Let's choose our previously created Digital Thing.

![Select Digital Thing to be instantiated](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/9-mount-new-container-select-dt.png "Select Digital Thing to be instantiated")

Also in this case, it is needed to enter a name and a description for the new Container.

Once inserted, IoT Catalyst Studio will add the Container to the IoT Domain and the Hypervisor on the remote host will be notified to mount it. This process may take a couple of minutes.

Once done, the editor will reopen in the folder that contains the newly mounted container. To make sure it instantiated the correct Digital Thing, let's open the IoT Catalyst DevOps Panel and check the contents of "Digital Thing Information": as expected, our Digital Thing has been correctly instantiated on the remote host.

![Digital Thing Information](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/12-digital-thing-info.png "Digital Thing Information")

We're now ready for the design phase of our Digital Twin!

## Adding Features to the Digital Thing

In the IoT Catalyst DevOps Panel, expand "Digital Thing Features". Here are listed all the features described in our Digital Thing's Bill of Features, grouped by category. We can see that no feature has been defined yet.

Now we'll add a dummy Data feature named "temperature". To do so, click on the "Add Feature" button at the top-right of the view.

![Add Feature](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/13-digital-thing-features-add.png "Add Feature")

A series of dialogs will guide us to create our new feature: let's choose "Data" as its type, and "temperature" as its name.

![Choose "Data" as the Feature Type](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/14-digital-thing-features-add-type.png "Choose \"Data\" as the Feature Type")

![Enter Feature Name](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/15-digital-thing-features-add-name.png "Enter Feature Name")

Since a temperature would be a numeric quantity, choose "numeric" as its Data Type.

![Choose Feature Data Type](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/16-digital-thing-features-add-datatype.png "Choose Feature Data Type")

As the final step, let's define the feature to be not updatable, since its values are not to be taken from the outside.

![Set the Feature as not updatable](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/17-digital-thing-features-add-updatable.png "Set the Feature as not updatable")

Done! You can see that our temperature has appeared in the "Data" section of the "Digital Thing Features" treeview.

![Feature has been added successfully](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/18-digital-thing-features-add-done.png "Feature has been added successfully")

Now let's see how the Digital Thing artifacts have been modified. By using the two highlighted icons as in the figure below, you can open the Digital Thing Bill of Features and its Python code respectively.

![Click the buttons to open Digital Thing Artifacts](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/19-digital-thing-features-add-open-documents.png "Click the buttons to open Digital Thing Artifacts")

Once clicked, we see that:
- The Bill of Features now includes a node representing the newly inserted feature.

- The Python code has been enriched with an autogenerated snippet which serves as the data provider for our temperature.

![The autogenerated sections are highlighted](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/20-digital-thing-features-add-results.png "The autogenerated sections are highlighted")

## Editing the Digital Thing Code

Now that we opened the Digital Thing main.py, let's add some code to generate random temperature values.

At the beginning of the code, add import for the `random` library:

```python
#****** INSERT HERE CUSTOM IMPORTS AND VARIABLES ********
import random
```

Then, replace the contents of `def temperature():` to match the following:

```python
_temperature=random.randint(10, 40)    
print('Producing data update for feature [temperature]',_temperature)
return _temperature
```

Now your code should look like this:

```python
# ...

#****** INSERT HERE CUSTOM IMPORTS AND VARIABLES ********
import random

# ...

@register_notification(auto=True,pool='main',post_procs=[],polling=settings['POLLING_TIME_DATA'],updatable=False)
def temperature():
    _temperature=random.randint(10, 40)    
    print('Producing data update for feature [temperature]',_temperature)
    return _temperature

# ...
```

Once done, save the document. A warning message should appear (see Figure below). This will synchronize changes in the code to the Bill of Features, if any. Click "Yes, Don't show again" to automatically synchronize the two documents in the future.

![Synchronization between Code and Bill of Features](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/21-digital-thing-features-sync.png "Synchronization between Code and Bill of Features")

## Notify IoT Catalyst about changes in the Digital Thing and its Container

Now that we've added some features to the Digital Thing, before debugging it, we should notify IoT Catalyst Studio about the changes in the Digital Thing and its mounted container. To do so, open the IoT Catalyst DevOps Panel and click on "Development Manager" > "Deploy Changes to IoT Catalyst".

![Deploy Changes to IoT Catalyst](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/22-deploy-changes.png "Deploy Changes to IoT Catalyst")

This will do the following:
- Apply changes to the Digital Thing in the IoT Catalyst library.
- Update the mounted container Bill of Features.

Once done, you should see a completion message like the following.

![Changes deployed successfully](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/23-deploy-changes-success.png "Changes deployed successfully")

## Debugging the Container

We're now ready to debug the Digital Thing code that is executed by the Container in the remote host.

To do so, open the IoT Catalyst DevOps Panel and click "Development Manager" > "Debug Container". 

![Debug Container](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/24-debug.png "Debug Container")

The extension creates a Python Debug Configuration and launches the Debugger. 

Let's now put a breakpoint at the end of the `def temperature():` function and watch the debugger stop at the defined breakpoint. You can notice that - in my case - the value generated for `temperature` is 11.

![Breakpoint in Digital Thing code](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/25-debug-session.png "Breakpoint in Digital Thing code")

You should also see this information message at the bottom-right of the editor.

![Open Container Details](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/26-open-container-details.png "Open Container Details")

By clicking on it, the Container details will be opened in IoT Catalyst Studio. On this webpage, go to the "Console" tab and verify that the last temperature value received by the IoT Platform is the same we saw while debugging.

![Container Output](https://gitlab.com/fairwinds/iotcatalystvscodeextension/-/raw/main/images/example-design-debug-deploy/27-container-details.png "Container Output")